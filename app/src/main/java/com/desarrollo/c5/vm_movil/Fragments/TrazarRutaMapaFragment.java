package com.desarrollo.c5.vm_movil.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.desarrollo.c5.vm_movil.PagoActivity;
import com.desarrollo.c5.vm_movil.R;
import com.desarrollo.c5.vm_movil.RutaActivity;
import com.desarrollo.c5.vm_movil.StreamingActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * Created by vrsa9208 on 20/07/16.
 */
public class TrazarRutaMapaFragment extends Fragment implements OnMapReadyCallback{

    private View view;
    public GoogleMap googleMap;
    public LatLngBounds bounds;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trazar_ruta_mapa,container,false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        RutaActivity activity = (RutaActivity) getActivity();
        map.moveCamera(CameraUpdateFactory.newLatLng(activity.startPoint));
        map.addMarker(new MarkerOptions()
                .position(activity.startPoint)
                .anchor(0.5f, 0.5f)
                .title(getResources().getString(R.string.btn_punto_inicial))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_start_end_point)));
        map.addMarker(new MarkerOptions()
                .position(activity.endPoint)
                .anchor(0.5f, 0.5f)
                .title(getResources().getString(R.string.btn_punto_final))
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_start_end_point)));

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Pattern pattern = Pattern.compile("[\\d]+$");
                if (pattern.matcher(marker.getTitle()).matches()) {
                    Intent intent = new Intent(getContext(), PagoActivity.class);
                    intent.putExtra(StreamingActivity.ID_STV, marker.getTitle());
                    startActivity(intent);
                }
                return true;
            }
        });

        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                bounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
            }
        });

        if(activity.dataCamResult != null){
            pintarRuta(activity.dataCamResult);
            pintarCamaras(activity.dataCamResult);
        }
    }

    private void pintarRuta(JSONObject dataCamResult){
        try {
            JSONArray points = dataCamResult.getJSONObject("content").getJSONArray("points");

            PolylineOptions polilyneOptions = new PolylineOptions()
                    .width(20)
                    .color(Color.argb(150, 22, 149, 163));

            for(int i = 0; i < points.length(); i++){
                JSONObject point = points.getJSONObject(i);
                polilyneOptions.add(new LatLng(point.getDouble("x"), point.getDouble("y")));
            }

            googleMap.addPolyline(polilyneOptions);
        } catch (JSONException e) {
            Log.e("TrazarRutaMapaFragment", e.getMessage());
        }
    }

    private void pintarCamaras(JSONObject dataCamResult){
        try {
            JSONArray camaras = dataCamResult.getJSONObject("content").getJSONArray("camaras");

            for(int i = 0; i < camaras.length(); i++){
                JSONObject cam = camaras.getJSONObject(i);
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(cam.getDouble("latitud"), cam.getDouble("longitud")))
                        .anchor(0.5f, 0.5f)
                        .title(cam.getString("id_stv"))
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_camera))
                        .snippet(getResources().getString(R.string.marker_snippet_camaras)));
            }

        } catch (JSONException e) {
            Log.e("TrazarRutaMapaFragment", e.getMessage());
        }
    }
}
