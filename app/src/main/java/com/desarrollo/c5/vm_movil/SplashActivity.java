package com.desarrollo.c5.vm_movil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    /**
     * Llave para leer el método de login en el archivo de preferencias
     */
    public static final String PREFERENCES_LOGIN_METHOD_KEY = "LOGIN_METHOD";

    /**
     * Tiempo antes de redireccionar hacia otra vista
     */
    private final int TIMER_DELAY = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        validateSession();
    }

    /**
     * Valida si el usuario ya tiene una sesión activa
     */
    private void validateSession() {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                getString(R.string.preferences_login_data_file), Context.MODE_PRIVATE);

        String loginMethod = sharedPref != null ? sharedPref.getString(PREFERENCES_LOGIN_METHOD_KEY, null) : null;
        Intent intent = null;

        //Si no tiene una sesión activa, se redireccionará al usuario a la
        //vista de Login
        if (loginMethod == null)
            intent = new Intent(getApplicationContext(), LoginActivity.class);
        //Si el usuario tiene una sesión activa, se redireccionará a la
        //vista principal
        else
            intent = new Intent(getApplicationContext(), MainActivity.class);

        changeActivityWithDelay(intent);
    }

    /**
     * Cambia de actividad después de un tiempo determinado
     * @param intent
     * Indica hacia que vista se redireccionará al usuario
     */
    private void changeActivityWithDelay(final Intent intent) {
        final TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        };

        //Inicialización y planificación del Timer
        final Timer timer = new Timer();
        timer.schedule(timerTask, 1000);
    }

}
