package com.desarrollo.c5.vm_movil.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.desarrollo.c5.vm_movil.R;

/**
 * Created by vrsa9208 on 19/07/16.
 */
public class DatosRegistroFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_datos_registro,container,false);

        return view;
    }
}
