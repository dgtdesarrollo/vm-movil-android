package com.desarrollo.c5.vm_movil.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.desarrollo.c5.vm_movil.MainActivity;
import com.desarrollo.c5.vm_movil.R;
import com.desarrollo.c5.vm_movil.RutaActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by vrsa9208 on 19/07/16.
 */
public class TrazarRutaFragment extends Fragment {



    private final int PLACE_PICKER_REQUEST_START_POINT = 1;
    private final int PLACE_PICKER_REQUEST_END_POINT = 2;
    private final int MY_SOCKET_TIMEOUT_MS = 15000;
    public static final String CAMERAS_REQUEST_TAG = "CAMERAS_REQUEST_TAG";

    private View view;
    private final TrazarRutaFragment self = this;

    private Button btnStartPoint;
    private Button btnEndPoint;
    private Button btnSiguiente;

    private Place startPoint;
    private Place endPoint;

    private Intent placePickerIntent;

    private ProgressBar progressBar;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trazar_ruta, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        try {
            placePickerIntent = new PlacePicker.IntentBuilder().build((Activity) self.getActivity());
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Log.e("TrazarRutaFragment", e.getMessage());
            Toast.makeText(view.getContext(), "Error fatal. Cierre programado", Toast.LENGTH_SHORT).show();
            self.getActivity().finish();
        }

        btnSiguiente = (Button) view.findViewById(R.id.btnSiguiente);
        getBtnSiguiente().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePostRequest();
            }
        });

        btnStartPoint = (Button) view.findViewById(R.id.btnStartPoint);
        btnStartPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBtnSiguiente().setVisibility(View.GONE);
                getProgressBar().setVisibility(View.VISIBLE);
                startActivityForResult(placePickerIntent, PLACE_PICKER_REQUEST_START_POINT);
            }
        });

        btnEndPoint = (Button) view.findViewById(R.id.btnEndPoint);
        btnEndPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBtnSiguiente().setVisibility(View.GONE);
                getProgressBar().setVisibility(View.VISIBLE);
                startActivityForResult(placePickerIntent, PLACE_PICKER_REQUEST_END_POINT);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getProgressBar().setVisibility(View.GONE);
        getBtnSiguiente().setVisibility(View.VISIBLE);

        switch(requestCode){
            case PLACE_PICKER_REQUEST_START_POINT:
                if (resultCode == Activity.RESULT_OK) {
                    Place place = PlacePicker.getPlace(data, (Activity) self.getActivity());
                    btnStartPoint.setText(place.getName());
                    startPoint = place;
                }
                else{
                    startPoint = null;
                    btnStartPoint.setText(getResources().getString(R.string.btn_punto_inicial));
                }
                break;
            case PLACE_PICKER_REQUEST_END_POINT:
                if (resultCode == Activity.RESULT_OK) {
                    Place place = PlacePicker.getPlace(data, (Activity) self.getActivity());
                    btnEndPoint.setText(place.getName());
                    endPoint = place;
                }
                else{
                    endPoint = null;
                    btnEndPoint.setText(getResources().getString(R.string.btn_punto_final));
                }
                break;
        }

        if(startPoint != null && endPoint != null){
            getBtnSiguiente().setEnabled(true);
        }
        else{
            getBtnSiguiente().setEnabled(false);
        }
    }

    private void makePostRequest(){
        ((MainActivity) getActivity()).activeCamerasRequest = true;
        getBtnSiguiente().setVisibility(View.GONE);
        getProgressBar().setVisibility(View.VISIBLE);

        final String URL = getResources().getString(R.string.url_cam_request);
        // Post params to be sent to the server
        HashMap<String, Double> params = new HashMap<String, Double>();
        params.put("c1Lat", startPoint.getLatLng().latitude);
        params.put("c1Lng", startPoint.getLatLng().longitude);
        params.put("c2Lat", endPoint.getLatLng().latitude);
        params.put("c2Lng", endPoint.getLatLng().longitude);

        JsonObjectRequest req = new JsonObjectRequest(URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ((MainActivity) getActivity()).activeCamerasRequest = false;
                        Log.i("Camaras Request", response.toString());
                        changeActivity(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getProgressBar().setVisibility(View.GONE);
                getBtnSiguiente().setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).activeCamerasRequest = false;
                VolleyLog.e("Error: ", error.getStackTrace().toString());
                Snackbar.make(getView(), R.string.error_peticion, Snackbar.LENGTH_LONG).setAction(R.string.reintentar, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        makePostRequest();
                    }
                }).show();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //Tag para detener la petición a petición del usuario
        req.setTag(CAMERAS_REQUEST_TAG);
        // add the request object to the queue to be executed
        ((MainActivity) getActivity()).mRequestQueue.add(req);
    }

    private void changeActivity(String data){
        getProgressBar().setVisibility(View.GONE);
        getBtnSiguiente().setVisibility(View.VISIBLE);
        Intent intent = new Intent(view.getContext(), RutaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(RutaActivity.CAM_RESULT, data);
        bundle.putDouble(RutaActivity.START_POINT_LAT, startPoint.getLatLng().latitude);
        bundle.putDouble(RutaActivity.START_POINT_LNG, startPoint.getLatLng().longitude);
        bundle.putDouble(RutaActivity.END_POINT_LAT, endPoint.getLatLng().latitude);
        bundle.putDouble(RutaActivity.END_POINT_LNG, endPoint.getLatLng().longitude);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    public Button getBtnSiguiente() {
        return btnSiguiente;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
