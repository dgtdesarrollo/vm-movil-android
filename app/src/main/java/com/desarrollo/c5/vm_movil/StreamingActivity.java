package com.desarrollo.c5.vm_movil;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

public class StreamingActivity extends AppCompatActivity {

    public static final String ID_STV = "ID_STV";
    public static final String TIEMPO_STREAMING = "TIEMPO_STREAMING";

    private String id_stv;
    private int minutos_streaming;
    public int contador_segundos = 0;

    private VideoView videoView;
    private TextView textTimerVideoStreaming;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        id_stv = getIntent().getExtras().getString(ID_STV);
        minutos_streaming = getIntent().getExtras().getInt(TIEMPO_STREAMING);

        videoView = (VideoView) findViewById(R.id.video_view_streaming);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                setTimer();
            }
        });
        textTimerVideoStreaming = (TextView) findViewById(R.id.text_timer_video_streaming);

        setVideoView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    private void setVideoView(){
        //String vidAddress = "http://187.174.224.153:1937/live/" + id_stv + ".stream/playlist.m3u8";
        String vidAddress = "http://187.174.224.153:1937/live/13982.stream/playlist.m3u8";
        Uri vidUri = Uri.parse(vidAddress);
        this.videoView.setVideoURI(vidUri);
        this.videoView.start();
    }

    private void setTimer(){

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                contador_segundos += 1;
                if(contador_segundos >= minutos_streaming * 60){
                    finish();
                }
                else{
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            new TextUpdater().execute();
                        }
                    }).start();
                }
            }
        };

        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 1000);
    }

    public class TextUpdater extends AsyncTask<String,String,Void>{

        @Override
        protected Void doInBackground(String... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            int segundosRestantes = minutos_streaming * 60 - contador_segundos;
            String minutos = ((int) Math.floor(segundosRestantes / 60)) > 9 ? "" + ((int) Math.floor(segundosRestantes / 60)) : "0" + ((int) Math.floor(segundosRestantes / 60));
            String segundos = segundosRestantes % 60 > 9 ? "" + segundosRestantes % 60 : "0" + segundosRestantes % 60;
            textTimerVideoStreaming.setText(minutos + " : " + segundos);
        }
    }
}
