package com.desarrollo.c5.vm_movil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.desarrollo.c5.vm_movil.Fragments.AvisoPrivacidadFragment;
import com.desarrollo.c5.vm_movil.Fragments.DatosRegistroFragment;
import com.desarrollo.c5.vm_movil.Fragments.FormaPagoFragment;
import com.desarrollo.c5.vm_movil.Fragments.HistorialPagosFragment;
import com.desarrollo.c5.vm_movil.Fragments.TerminosCondicionesFragment;
import com.desarrollo.c5.vm_movil.Fragments.TrazarRutaFragment;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean inicio = true;

    public boolean activeCamerasRequest = false;
    public RequestQueue mRequestQueue;
    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(inicio){
            navigationView.getMenu().getItem(3).setChecked(true);
            selectItem(new TrazarRutaFragment());
            inicio = false;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START) || activeCamerasRequest) {
            if (drawer.isDrawerOpen(GravityCompat.START)){
                drawer.closeDrawer(GravityCompat.START);
            }
            else if(activeCamerasRequest){
                TrazarRutaFragment trazarRutaFragment = (TrazarRutaFragment) currentFragment;
                mRequestQueue.cancelAll(TrazarRutaFragment.CAMERAS_REQUEST_TAG);
                trazarRutaFragment.getProgressBar().setVisibility(View.GONE);
                trazarRutaFragment.getBtnSiguiente().setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), R.string.trazar_ruta_cancelada, Toast.LENGTH_SHORT).show();
                activeCamerasRequest = false;
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.menu_configuracion:
                Toast.makeText(getApplicationContext(), "En construcción", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_acerca_de:
                Snackbar.make(getCurrentFocus(), "App desarrollada por vrsa9208", Snackbar.LENGTH_LONG).setAction(R.string.cerrar, null).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.id_menu_trazar_ruta:
                selectItem(new TrazarRutaFragment());
                break;
            case R.id.id_menu_datos_registro:
                selectItem(new DatosRegistroFragment());
                break;
            case R.id.id_menu_forma_pago:
                selectItem(new FormaPagoFragment());
                break;
            case R.id.id_menu_historial_pagos:
                selectItem(new HistorialPagosFragment());
                break;
            case R.id.id_menu_datos_personales:
                selectItem(new AvisoPrivacidadFragment());
                break;
            case R.id.id_menu_terminos_condiciones:
                selectItem(new TerminosCondicionesFragment());
                break;
            case R.id.id_menu_cerrar_sesion:
                finishSession();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectItem(Fragment fragment) {
        this.currentFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_content, fragment)
                .commit();
    }

    private void finishSession(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                getString(R.string.preferences_login_data_file), Context.MODE_PRIVATE);

        if(sharedPref != null && sharedPref.getString(SplashActivity.PREFERENCES_LOGIN_METHOD_KEY, null).equals(LoginActivity.FB_LOGIN)){
            LoginManager.getInstance().logOut();
        }

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SplashActivity.PREFERENCES_LOGIN_METHOD_KEY, null);
        editor.commit();

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
