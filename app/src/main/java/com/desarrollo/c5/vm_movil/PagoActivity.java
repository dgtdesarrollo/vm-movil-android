package com.desarrollo.c5.vm_movil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class PagoActivity extends AppCompatActivity implements View.OnClickListener{

    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private RadioButton radioButton4;
    private RadioButton selectedRadio = null;
    private String id_stv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        id_stv = getIntent().getExtras().getString(StreamingActivity.ID_STV);

        radioButton1 = (RadioButton) findViewById(R.id.compra_minutos_radio_1);
        radioButton2 = (RadioButton) findViewById(R.id.compra_minutos_radio_2);
        radioButton3 = (RadioButton) findViewById(R.id.compra_minutos_radio_3);
        radioButton4 = (RadioButton) findViewById(R.id.compra_minutos_radio_4);

        radioButton1.setOnClickListener(this);
        radioButton2.setOnClickListener(this);
        radioButton3.setOnClickListener(this);
        radioButton4.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    public void regresar(View view){
        finish();
    }

    public void avanzar(View view){
        if(selectedRadio == null){
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.mensaje_compra_selecciona_pago),
                    Toast.LENGTH_SHORT).show();
        }else {
            Intent intent = new Intent(getApplicationContext(), StreamingActivity.class);
            intent.putExtra(StreamingActivity.ID_STV, id_stv);
            switch(selectedRadio.getId()){
                case R.id.compra_minutos_radio_1:
                    intent.putExtra(StreamingActivity.TIEMPO_STREAMING, 1);
                    break;
                case R.id.compra_minutos_radio_2:
                    intent.putExtra(StreamingActivity.TIEMPO_STREAMING, 2);
                    break;
                case R.id.compra_minutos_radio_3:
                    intent.putExtra(StreamingActivity.TIEMPO_STREAMING, 3);
                    break;
                case R.id.compra_minutos_radio_4:
                    intent.putExtra(StreamingActivity.TIEMPO_STREAMING, 5);
                    break;
            }
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        radioButton1.setChecked(false);
        radioButton2.setChecked(false);
        radioButton3.setChecked(false);
        radioButton4.setChecked(false);
        selectedRadio = (RadioButton) v;
        selectedRadio.setChecked(true);
    }
}
