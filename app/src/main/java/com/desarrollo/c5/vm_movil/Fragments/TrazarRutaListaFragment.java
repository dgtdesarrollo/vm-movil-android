package com.desarrollo.c5.vm_movil.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.desarrollo.c5.vm_movil.PagoActivity;
import com.desarrollo.c5.vm_movil.R;
import com.desarrollo.c5.vm_movil.RutaActivity;
import com.desarrollo.c5.vm_movil.StreamingActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vrsa9208 on 20/07/16.
 */
public class TrazarRutaListaFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trazar_ruta_lista,container,false);
        RutaActivity activity = (RutaActivity) getActivity();

        if(activity.dataCamResult != null){
            final ArrayList<CamaraList> list = readCameras(activity.dataCamResult);

            ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_2, android.R.id.text1, list) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                    TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                    text1.setText(list.get(position).getId_stv());
                    text2.setText(list.get(position).getDireccion());
                    return view;
                }
            };

            final ListView listView = (ListView) view.findViewById(R.id.listView);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CamaraList camSelected = list.get(position);
                    Intent intent = new Intent(getContext(), PagoActivity.class);
                    intent.putExtra(StreamingActivity.ID_STV, camSelected.id_stv);
                    startActivity(intent);
                }
            });
        }

        return view;
    }

    private ArrayList<CamaraList> readCameras(JSONObject dataCamResult){
        ArrayList<CamaraList> list = new ArrayList<CamaraList>();

        try {
            JSONArray camaras = dataCamResult.getJSONObject("content").getJSONArray("camaras");

            for(int i = 0; i < camaras.length(); i++){
                JSONObject cam = camaras.getJSONObject(i);
                list.add(new CamaraList(cam.getString("id_stv"), cam.getString("direccion")));
            }

        } catch (JSONException e) {
            Log.e("TrazarRutaMapaFragment", e.getMessage());
        }

        return list;
    }

    public class CamaraList{

        private String id_stv;
        private String direccion;

        public CamaraList(String id_stv, String direccion){
            this.setId_stv(id_stv);
            this.setDireccion(direccion);
        }


        public String getId_stv() {
            return id_stv;
        }

        public void setId_stv(String id_stv) {
            this.id_stv = id_stv;
        }

        public String getDireccion() {
            return direccion;
        }

        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }
    }
}
