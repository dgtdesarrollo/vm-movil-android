package com.desarrollo.c5.vm_movil;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.desarrollo.c5.vm_movil.Fragments.TrazarRutaListaFragment;
import com.desarrollo.c5.vm_movil.Fragments.TrazarRutaMapaFragment;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class RutaActivity extends AppCompatActivity {

    public static final String CAM_RESULT = "CAM_RESULT";
    public static final String START_POINT_LAT = "START_POINT_LAT";
    public static final String START_POINT_LNG = "START_POINT_LNG";
    public static final String END_POINT_LAT = "END_POINT_LAT";
    public static final String END_POINT_LNG = "END_POINT_LNG";

    private boolean modoMapa = true;
    private FloatingActionButton fab;
    public JSONObject dataCamResult;
    public LatLng startPoint;
    public LatLng endPoint;

    TrazarRutaMapaFragment mapaFragment;
    TrazarRutaListaFragment listaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruta);

        readExtras();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(modoMapa){
                    mostrarModoLista();
                }
                else{
                    mostrarModoMapa();
                }
                modoMapa = !modoMapa;
            }
        });

        mapaFragment = new TrazarRutaMapaFragment();
        listaFragment = new TrazarRutaListaFragment();
        mostrarModoMapa();
    }

    private void mostrarModoMapa(){
        fab.setImageResource(R.drawable.ic_list);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.rutaMainContent, mapaFragment)
                .commit();
    }

    private void mostrarModoLista(){
        fab.setImageResource(R.drawable.ic_map);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.rutaMainContent, listaFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }

        return true;
    }

    private void readExtras(){
        Bundle bundle = getIntent().getExtras();
        try {
            dataCamResult = new JSONObject(bundle.getString(RutaActivity.CAM_RESULT));
        } catch (JSONException e) {
            Log.e("RutaActivity", e.getMessage());
        }
        startPoint = new LatLng(bundle.getDouble(RutaActivity.START_POINT_LAT), bundle.getDouble(RutaActivity.START_POINT_LNG));
        endPoint = new LatLng(bundle.getDouble(RutaActivity.END_POINT_LAT), bundle.getDouble(RutaActivity.END_POINT_LNG));
    }

}
